/*
 * Together these functions manage the creation and definition of
 * the so-called "bubbles." The bubbles are our point of connection between
 * the world map interface and individual articles
 *
*/

/*
 * Merges two dictionaries
 */
 
function mergeDicts(dictOne, dictTwo) {
            mergedDict = {};
            for (var attrname in dictOne) {
                mergedDict[attrname] = dictOne[attrname];
            }
            for (var attrname in b) {
                mergeDict[attrname] = dictTwo[attrname];
            }
            return mergedDict;
}


/*
 * Need to look into this one
 */

function redrawBubbles() {
    //bubbles, custom popup on hover template
    map.bubbles(bubblesArray, {popupTemplate: hoverInfo});
}

/*
 * Creates a new bubble using the parsed JSON
 * data for the article as well as the retrieved
 * coordinates for the supplied address
 */
function addBubble(bubbleJSON, lat, lon) {
    // Creates a new bubble (not sure why 2 are created) with the latitude and longitude
    newBubble = {latitude: lat, longitued: lon, raduis: BUBBLE_SIZE, fillKey: 'gt51'};
    newBubble = {latitude: lat, longitude: lon, radius: BUBBLE_SIZE, fillKey: 'gt50'};
    // Uses mergedDicts to add the JSON data from bubbleJSON to the bubble object
    newBubble = mergeDicts(newBubble, bubbleJSON);
    // Adds the newly created bubble to our array of bubbles
    bubblesArray.push(newBubble);
    // Refreshes the entire set of bubbles 
    // to make the newely created bubble appear
    redrawBubbles();
}



 /*
  * Takes the extracted address from the article metadata
  * and looks up the coordinates of that address using the Google Maps API
  * Our future version should cache known addresses in our own database and do a lookup there first
  * If the lookup fails, grab the data from the Google API and add it to the database
  */
function addBubbleAtAddress(bubbleJSON, addressString) {
    geocoder.geocode({address: addressString}, function (results, status) {
        // If the address lookup succeeds, grab the latitude and longitude and store them
        if (status == google.maps.GeocoderStatus.OK) {
            lat = results[0].geometry.location.lat();
            lon = results[0].geometry.location.lng();
            
            // Create a dictionary with the address as a string as well as the latitude and longitude
            // Not sure why we put the latitude in the dictionary as well as as an argument in addBubble
            locationDictionary = {location: addressString};
            updatedLocationDict = mergeDicts(locationDictionary, bubbleJSON);
            
            // Add a bubble with the address dictionary, latitude and longitude 
            // (still unsure of why all 3 are needed)
            addBubble(updatedLocationDict, lat, lon);
        } 
        
        // If the call fails, print to console the reasons
        // Perhaps we'll also keep a databade of call fails so we can 
        // diagnose problems remotely
        else {
            console.log("Geocode was not successful on address string: " + addressString + " for the following reason: " + status);
        }
    });
}

/*
 * This function handles the generation of the actual popup window
 * that appears when a user hovers (or clicks on) an article bubble
 */
function hoverInfo(geo, data) {
    // Grabs the handlebars template for the popup
    var source = $("#hoverTemplate").html(); 
    // Has Handlebars.js compile the template
    var template = Handlebars.compile(source);
    // Returns the compiled template
    return template(data);
}   
